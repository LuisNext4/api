const axios = require('axios')
require('dotenv').config()

const url = process.env.URL
const params = {
    action: "GetTickets",
    user: process.env.IDENTIFIER,
    password: process.env.SECRET,
    accesskey: process.env.KEY

}

const getTickets = async () => {
    const treatedTickets = []
    try {
        const response =  await axios.post(`${url}?action=${params.action}&identifier=${params.user}&secret=${params.password}&accesskey=${params.accesskey}&responsetype=json`); 
        const ticketsArray = response.data.tickets
        ticketsArray.ticket.map(ticket => {
            // console.log(ticket);
            const ticketInfo = {
                ticketSearchId: ticket.id,
                ticketId: ticket.tid,
                userid: ticket.userid,
                userName: ticket.name,
                userEmail: ticket.email,
                subject: ticket.subject,
                status: ticket.status,
                priority: ticket.priority,
                lastreply: ticket.lastreply,
            }
             treatedTickets.push(ticketInfo)
            //  console.log(treatedTickets)
        })
        return treatedTickets
    } catch (error) {
        console.log(error)
    }
}



exports.getTickets = getTickets


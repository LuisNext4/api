const axios = require('axios')
require('dotenv').config()

const url = process.env.URL
const params = {
    action: "GetClients",
    user: process.env.IDENTIFIER,
    password: process.env.SECRET,
    accesskey: process.env.KEY

}

const getClients = async () => {
    try {
        const response =  await axios.post(`${url}?action=${params.action}&identifier=${params.user}&secret=${params.password}&accesskey=${params.accesskey}&responsetype=json&limitnum=500`); 
        const data = response.data.clients
        return data
    } catch (error) {
        return error
    }
}


exports.getClients = getClients

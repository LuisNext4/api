const { addItem } = require('../notion/addToDatabase')
const { getTickets } = require('../tickets/getTickets')

const getDataBase = require('../notion/getDatabase');

const refreshTickets = (ticketArray) => {
    const ticketIdArray = []
    // console.log(ticketIdArray)
    
    ticketArray.map(ticket => {
        ticketIdArray.push(ticket.ticketId)
    })
    // console.log(ticketIdArray)

    return ticketIdArray
}


const refreshNotionTickets = (ticketArray) => {
    const ticketIdArray = []

    ticketArray.map(ticket => {
        ticketIdArray.push(ticket.properties.Ticket.rich_text[0].plain_text)
    })

    return ticketIdArray
}

const checkTickets = () => {
    async function  checkTicket() {
        const ticketsArray = await getTickets()
        const NotionTicketArray = await getDataBase()
        
        const ticketIdArray = refreshTickets(ticketsArray)
        const NotionTicketIdArray = refreshNotionTickets(NotionTicketArray)

        ticketIdArray.map(id => {
            if(NotionTicketIdArray.indexOf(id) > -1){
            }else{
                addItem(ticketsArray[ticketIdArray.indexOf(id)])
            }
        })
        
     }
     setInterval(checkTicket, 5000);
    
    
     
}


module.exports = checkTickets

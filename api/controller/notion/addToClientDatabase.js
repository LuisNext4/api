require('dotenv').config()
const {Client} =  require('@notionhq/client')
const { text } = require('express')
const { getTickets } = require('../tickets/getTickets')

const notion = new Client({
    auth: process.env.NOTION_KEY
})
const databaseId = process.env.NOTION_CLIENT_DATABASE_ID


async function addItem(user) {
    try {
      await notion.request({
        path: "pages",
        method: "POST",
        body: {
          parent: { database_id: databaseId },
          properties: {
           
            Email: {
              "type": "email",
              email: user.email
            },
            ID: {
              "type": "number",
              number: user.id
               
            },
            Nome: {
              "type": "rich_text",
              rich_text: [{
                  type: 'text',                  
                  text: {
                    content: user.firstname
                  }
                  
              }]
               
            },
            Empresa: {
              "type": "title",
              title: [
                {
                  type: text,
                  text: {
                    content: user.companyname
                  }
                }
              ]
            },
            
            
          }
        },
      })
      console.log("User ID",user.id," -  adicionado")
    } catch (error) {
      console.error(error)
    }
   
   
}


exports.addItem = addItem
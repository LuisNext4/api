
require('dotenv').config()
const {Client} =  require('@notionhq/client')


const notion = new Client({
    auth: process.env.NOTION_KEY
})
const databaseId = process.env.NOTION_CLIENT_DATABASE_ID




const getClient = async (id) => {
  const client = []
    const response = await notion.databases.query({
      database_id: databaseId
    })
    const databaseItems = response.results
    databaseItems.map(clientInfo => {
      if(clientInfo.properties.ID.number == id){
        client.push(clientInfo)
      }
    })
    return client
    
};
  
module.exports = getClient
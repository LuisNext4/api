
require('dotenv').config()
const {Client} =  require('@notionhq/client')


const notion = new Client({
    auth: process.env.NOTION_KEY
})
const databaseId = process.env.NOTION_DATABASE_ID





const getDataBase = async () => {
  
    const response = await notion.databases.query({
      database_id: databaseId
    })
    const databaseItems = response.results
    // console.log(databaseItems)
    return databaseItems
};
  
module.exports = getDataBase
require('dotenv').config()
const {Client} =  require('@notionhq/client')
const { urlencoded } = require('express')
const { getTickets } = require('../tickets/getTickets')

const notion = new Client({
    auth: process.env.NOTION_KEY
})
const databaseId = process.env.NOTION_DATABASE_ID
const getTicketUrl = (id) => {
  const baseUrl = process.env.TICKET_URL
  const url = `${baseUrl}?action=view&id=${id}`
  return url
}


async function addItem(ticket) {
    try {
      await notion.request({
        path: "pages",
        method: "POST",
        body: {
          parent: { database_id: databaseId },
          properties: {
            "Descrição": {
                type: "title",
                title: [{ 
                    "type": "text",
                    "text": {
                    "content": ticket.subject 
                    } 
                    }
                ]
            },
            Ticket: {
              "type": "rich_text",
              rich_text: [{
                  type: 'text',                  
                  text: {
                    content: ticket.ticketId
                  }
                  
              }]
            },
            Requestor: {
              "type": "rich_text",
              rich_text: [{
                  type: 'text',                  
                  text: {
                    content: ticket.userName
                  }
                  
              }]
               
            },
            Status: {
              "type": "rich_text",
              rich_text: [{
                  type: 'text',                  
                  text: {
                    content: ticket.status
                  }
                  
              }]
               
            },
            "Mês": {
              "type": "date",
              date: {
                start: ticket.lastreply,
                end: null
              }
            },
            "Ticket Link": {
              type: "url",
              "url": getTicketUrl(ticket.ticketSearchId)
            }
            
          }
        },
      })
      console.log("Ticket id - ",ticket.ticketId, "adicionado")
    } catch (error) {
      console.error(error)
    }
   
   
}


exports.addItem = addItem

require('dotenv').config()
const {Client} =  require('@notionhq/client')


const notion = new Client({
    auth: process.env.NOTION_KEY
})
const databaseId = process.env.NOTION_DATABASE_ID




const getTicket = async (id) => {
    const ticket = []
  
    const response = await notion.databases.query({
      database_id: databaseId
    })
    const databaseItems = response.results
    databaseItems.map(TicketInfo => {
      if(TicketInfo.properties.Ticket.rich_text[0].plain_text == id){
        ticket.push(TicketInfo)
      }
    })
    return ticket
    
};
  
module.exports = getTicket
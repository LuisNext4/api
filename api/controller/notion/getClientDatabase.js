
require('dotenv').config()
const {Client} =  require('@notionhq/client')


const notion = new Client({
    auth: process.env.NOTION_KEY
})
const databaseId = process.env.NOTION_CLIENT_DATABASE_ID





const getClientsDatabase = async () => {
  
    const response = await notion.databases.query({
      database_id: databaseId
    })
    const databaseItems = response.results
    return databaseItems
};
  
module.exports = getClientsDatabase
const express = require('express')
const { getTickets } = require('./api/controller/tickets/getTickets')
const { getClients } = require('./api/controller/tickets/getClients')
const getDataBase = require('./api/controller/notion/getDatabase');

const checkTickets = require('./api/controller/verify/checkTickets')
const checkUsers = require('./api/controller/verify/checkClients');
const getClientsDatabase = require('./api/controller/notion/getClientDatabase');
const getClient = require('./api/controller/notion/getClient');
const getTicket = require('./api/controller/notion/getTicket');


const app = express()
const port = process.env.PORT || 5000


checkTickets()
checkUsers()

 app.get('/', (req,res) => {
    
    res.send('Up And Running 🎈');
 })
 app.get('/getTickets', (req, res) => {
      getTickets().then(
        function(tickets){
          // console.log(tickets)
          res.send(tickets)

        }
      )
      
 })
 app.get('/getTicketsDatabase', (req,res) => {

    getDataBase().then(
      function(database){
        res.send(database)
      }
    )
 })
 app.get('/getNotionClients/:id', (req,res) => {
    getClient(req.params.id).then(
      function(client){
        res.send(client)
      }
    )
 })
 app.get('/getNotionTickets/:id', (req,res) => {
    
    getTicket(req.params.id).then(
      function(ticket){
        res.send(ticket)
      }
    )
 })
 app.get('/getNotionClients', (req,res) => {

    getClientsDatabase().then(
      function(database){
        res.send(database)
      }
    )
 })
app.get('/getClients', (req, res) => {
  
  getClients().then(
    function(clients){
      res.send(clients)
    }
  )
})

app.listen(port, () => {
  console.log(`Listening on ${ port }`)
})
